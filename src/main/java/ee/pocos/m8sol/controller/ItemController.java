package ee.pocos.m8sol.controller;

import ee.pocos.m8sol.model.Item;
import ee.pocos.m8sol.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    /**
     * Get list of items
     * **/
    @GetMapping("/items")
    public @ResponseBody
    List<Item> getAllItems() {
        return itemService.list();
    }

    /**
     * Create or update item
     * **/
    @PostMapping("/items")
    public @ResponseBody void save(@RequestBody Item item) { itemService.save(item);}

    /**
     * Delete item by id
     * **/
    @DeleteMapping("items/{itemId}")
    public void deleteUnitById(@PathVariable Long itemId){
        itemService.delete(itemId);
    }
}
