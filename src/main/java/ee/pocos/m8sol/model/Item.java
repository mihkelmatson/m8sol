package ee.pocos.m8sol.model;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Items")
public class Item {

    @Id
    @GeneratedValue
    @Column(name = "itemid")
    private Long itemId;

    @Column(name = "one")
    private String one;

    @Column(name = "two")
    private String two;

    @Column(name = "three")
    private String three;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }



}
