package ee.pocos.m8sol.service;

import ee.pocos.m8sol.dao.ItemDao;
import ee.pocos.m8sol.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemServiceImp implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Transactional
    public void save(Item item) {
        itemDao.save(item);
    }

    @Transactional(readOnly = true)
    public List<Item> list() {

        return itemDao.list();
    }

    @Transactional
    public void update(Item item) {
        itemDao.save(item);
    }

    @Transactional
    public void delete(Long id) {
        itemDao.delete(id);
    }
}
