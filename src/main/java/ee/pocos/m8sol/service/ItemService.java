package ee.pocos.m8sol.service;

import ee.pocos.m8sol.model.Item;

import java.util.List;


public interface ItemService {

    void save(Item item);
    List<Item> list();
    void update (Item item);
    void delete(Long id);
}
