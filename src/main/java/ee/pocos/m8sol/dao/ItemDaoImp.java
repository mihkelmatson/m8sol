package ee.pocos.m8sol.dao;

import ee.pocos.m8sol.model.Item;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;


@Repository
public class ItemDaoImp implements ItemDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Save/create Item object
     * **/
    @Transactional
    public void save(Item item) {

        if(item.getItemId() == null) {
            sessionFactory.getCurrentSession().save(item);
        } else {
            sessionFactory.getCurrentSession().merge(item);
        }

    }

    /**
     * Delete existing Item object
     * **/
    @Override
    public void delete(Long id){
        Item itm = new Item();
        itm.setItemId(id);
        sessionFactory.getCurrentSession().delete(itm);
    }

    @Override
    public List<Item> list() {
        @SuppressWarnings("unchecked")
        TypedQuery<Item> query = sessionFactory.getCurrentSession().createQuery("from Item");
        return query.getResultList();
    }
}
