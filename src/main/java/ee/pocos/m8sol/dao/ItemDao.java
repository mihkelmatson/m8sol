package ee.pocos.m8sol.dao;

import ee.pocos.m8sol.model.Item;

import java.util.List;

public interface ItemDao {

    void save(Item item);
    List<Item> list();
    void delete(Long itemId);
}
