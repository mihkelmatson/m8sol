(function () {
    'use strict';
    angular.module('app', ['ngRoute', 'angularModalService', 'ngAnimate']);
})();