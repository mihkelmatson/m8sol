(function () {
    'use strict';

    angular.module('app').config(Conf);

    function Conf($routeProvider){

        $routeProvider.when('/',{
            templateUrl : 'views/items.html',
            controller : 'ItemCtrl',
            controllerAs : 'vm'
        }).otherwise('/');
    }

})();