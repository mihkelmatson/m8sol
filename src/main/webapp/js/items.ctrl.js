(function () {
    'use strict';

    angular.module('app').controller('ItemCtrl', Ctrl);

    function Ctrl($http, $location, ModalService) {

        var vm = this;
        vm.items = [];


        vm.deleteItem = deleteItem;
        vm.createItem = createItem;
        //vm.editItem = editItem;
        vm.showCustom = showCustom;

        init();

        function init(){
            $http.get('http://localhost:8080/items').then(function (result){
                vm.items = result.data;
            });
        }

        function createItem() {
            var newItem = {
                itemId:11,
                one: "7one",
                two: "7two",
                three: "7three"
            };

            modalService.confirm().then(function() {
                return $http.post('http://localhost:8080/items', newItem);
            }).then(init);

        }

        // function editItem(item) {
        //     //console.log(item);
        //     modalService.edit(item);
        // }

        function showCustom() {
            console.log("editItem");
            ModalService.showModal({
                templateUrl: "/view/modal.html",
                controller: "CustomController",
                bodyClass: "custom-modal-open"
            }).then(function(modal) {
                modal.close.then(function(result) {
                    console.log("All good!");
                });
            });
        }

        function deleteItem(id){
                $http.delete('http://localhost:8080/items/'+id).then(function (result){
                   init();
                });
        }
    }

})();