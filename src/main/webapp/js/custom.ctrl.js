(function () {
    'use strict';

    angular.module('app').controller('CustomController', Ctrl);
    function Ctrl($scope, close) {
        $scope.close = close;
    }
})();